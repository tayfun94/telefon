import java.util.ArrayList;

public class MobilePhone {
	private ArrayList<Contact> contacts;

	public MobilePhone(ArrayList<Contact> contacts) {
		this.contacts = contacts;
	}

	public MobilePhone() {
		contacts = new ArrayList<>();
	}

	public void addContanct(Contact contact) {
		if (findContactPosition(contact.getName()) >= 0) {
			System.out.println("Bastanul deja exista");

		} else {
			this.contacts.add(contact);
			System.out.printf("Cocalar adaugat in lista");
		}
	}

	public void removeContact(Contact contact) {
		contacts.removeIf(contact1 -> contact1.getName().equals(contact.getName()));
	}

	public void modifyContact(Contact contact) {
		contacts.stream().filter(cont -> cont.getName().equals(contact.getName())).findFirst().ifPresent(cont -> cont.setNumber(contact.getNumber()));
		contacts.stream().filter(cont -> cont.getNumber() == contact.getNumber()).findFirst().ifPresent(cont -> cont.setName(contact.getName()));
	}


	public int findContactPosition(String name) {
		for (int i = 0; i < this.contacts.size(); i++) {
			if (this.contacts.get(i).getName().equals(name)) {
				return i;
			}
		}
		return -1;
	}

	public int findContactPositionByNumber(long number) {
		for (int i = 0; i < this.contacts.size(); i++) {
			if (this.contacts.get(i).getNumber() == number) {
				return i;
			}
		}
		return -1;
	}

	public void findContact(String contactName) {
		int position = findContactPosition(contactName);
		if (position >= 0) {
			System.out.println("Nume: " + contacts.get(position).getName() + " Numar: " + contacts.get(position).getNumber());
		}
		System.out.println("Lista ta e goala, mancatias, nai valoare");
	}


	public void printContactList() {
		System.out.println("Lista de smardoi");
		for (Contact contact : this.contacts) {
			System.out.println(contact.getName() + " " + contact.getNumber());
		}
	}
}
