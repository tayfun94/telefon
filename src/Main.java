import java.util.Scanner;

public class Main {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		Main main = new Main();
		MobilePhone mobilePhone = new MobilePhone();

		boolean exit = false;

		printTheListOfActions();

		while (!exit) {
			System.out.println("\n Da-mi o actiune : (Sau 6 pentru lista)");
			int action = scanner.nextInt();

			switch (action) {
				case 0:
					System.out.println("\nSe inchide ...");
					exit = true;
					break;

				case 1:
					mobilePhone.printContactList();
					break;

				case 2:
					mobilePhone.addContanct(main.addContanct());
					break;

				case 3:
					mobilePhone.modifyContact(main.updateContact());
					break;

				case 4:
					mobilePhone.removeContact(main.removeContact());
					break;

				case 5:
					mobilePhone.findContact(main.queryContact());
					break;

				case 6:
					printTheListOfActions();
					break;

				default:
					throw new IllegalStateException("Numere intre 0 si 6 ");
			}

		}
	}

	private Contact removeContact() {
		return addContanct();
	}

	private Contact updateContact() {
		return addContanct();
	}

	private String queryContact() {
		scanner.nextLine();
		System.out.println("Da-mi un nume de contact care deja exista");
		return scanner.nextLine();
	}

	private Contact addContanct() {
		scanner.nextLine();
		System.out.println("Da-mi un nume de contact");
		String name = scanner.nextLine();
		System.out.println("Da-mi un numar de contact");
		long number = scanner.nextLong();
		return new Contact(name, number);
	}

	private static void printTheListOfActions() {
		System.out.println("\nActiuni mancati-as, apasa pe ele");
		System.out.println("0  - Ma inchizi? Naspa\n" +
						"1  - Arata valorile\n" +
						"2  - Baga un valoros\n" +
						"3  - Modifica un cocalar\n" +
						"4  - Sterge un cocalar fara bani din lista\n" +
						"5  - Verifica daca smardoiul este in lista\n" +
						"6  - Arata toate actiune.");
		System.out.println("Alege ca un rege. ");
	}
}
